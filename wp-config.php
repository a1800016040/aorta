<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'aorta' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-!+Ov^6^y*>/ 42Y%lUoVR9;~:(96JR#{nw%E~)-[V]hq%HXL^t_55HCV@YaKq;6' );
define( 'SECURE_AUTH_KEY',  'q=X;i9Ot~x=1Ys7K)yo(FDL&s.cKA_1315M{NX)eO>$eC G!H%|%~KC#Z#fXYUXD' );
define( 'LOGGED_IN_KEY',    '0H1BwDf 4>+w+lX|`EKJRMc|{P*3xSbBk4{69RL@0oOYYd5CN?Hg9b<X|m4leay_' );
define( 'NONCE_KEY',        'u,Msz$@!GJ1O0z1,TeC P(2!_c|w NK|v!*!d?dzT)3cMMMAAO 1W7aslq4~2uym' );
define( 'AUTH_SALT',        'vN8mOhFlyRx/m&}dYt_.*?Ofh:`PqD-Y;E,F4*mPQ2Tn/gsvm5jj@ET/$F&GHuwu' );
define( 'SECURE_AUTH_SALT', 'M+>4b9QL),w`N}hJX`G)Y!Y+;0ww&jz0Hzx?!}qgXH~H{vKlrubS`n9]B/EfcE2x' );
define( 'LOGGED_IN_SALT',   'rVFj#Hh(87ZG,(fv6?`H#ax!dV$Q`s5i!jS>`ry?CLs-~6EZB%< y#[/0zjFZ}YU' );
define( 'NONCE_SALT',       'v_(9Yz?+ZHq);bw(qw{]u2/O,hf/jxxne#A< k^gA;S|A4nB93n KMIXypiNeyD[' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
